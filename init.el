(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

;; Set package and install
(defvar my-packages '(ace-jump-mode
                      expand-region
                      markdown-mode
                      magit)
  "A list of packages to ensure are installed at launch.")
(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

;; Disable bars
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)
;; Matching parenthesis is highlighted
(show-paren-mode t)
;; Indentation can't insert tabs
(setq-default indent-tabs-mode nil)
(setq x-select-enable-clipboard t
      x-select-enable-primary t
      split-width-threshold 0)
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory
                                               "backups"))))
(winner-mode t)
(ido-mode t)

(autoload
  'ace-jump-mode
  "ace-jump-mode"
  "Emacs quick move minor mode"
  t)
(define-key global-map (kbd "C-;") 'ace-jump-mode)

(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

(global-set-key (kbd "C-c g") 'magit-status)

;; Functions

(defun kill-all-buffers ()
  "kill all buffers, leaving *scratch* only"
  (interactive)
  (mapcar (lambda (x) (kill-buffer x))
	  (buffer-list))
  (delete-other-windows))
